﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Essentials;
using Android.App;
using Android.Content;
using Android.Gms.Location;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MemoryMap.Resources.Logic.Repositories;
using MemoryMap.Resources.Logic.Models;
using MemoryMap.Resources.Logic;
using Android.Media;
using Android.Support.V4.Media;
using Android.Support.V4.App;
using Xamarin.Android.Net;



namespace MemoryMap
{
    [BroadcastReceiver]
    public class MMLocationBroadcastReceiver : BroadcastReceiver
    {
        public static string ACTION_PROCESS_LOCATION = "MemoryMap.UPDATE_LOCATION";

        private NotesRepository _notesRepository = MainActivity.GetInstance().notes;

        public override void OnReceive(Context context, Intent intent)
        {

            string action = intent.Action;
            if (action.Equals(ACTION_PROCESS_LOCATION))
            {
                LocationResult locationResult = LocationResult.ExtractResult(intent);
                if (locationResult != null)
                {
                    var closest_locations = _notesRepository.Items
                        .Where(note => (Location.CalculateDistance(note.Location, new Location(locationResult.LastLocation.Altitude, locationResult.LastLocation.Longitude), DistanceUnits.Kilometers) < 0.2f))
                        .OrderBy(note => (Location.CalculateDistance(note.Location, new Location(locationResult.LastLocation.Altitude, locationResult.LastLocation.Longitude), DistanceUnits.Kilometers)));

                    try
                    
                        {
                            Toast.MakeText(context, $"Remainder detected {closest_locations.ElementAt(0).Title}", ToastLength.Long).Show();
                        }
                        catch (System.ArgumentOutOfRangeException)
                        {
                            Toast.MakeText(context, $"Received Location! {locationResult.LastLocation.Latitude}/{locationResult.LastLocation.Longitude}", ToastLength.Short).Show();
                        }
                }
/*Не надо
                var channelName = "Memory Map";
                var channelDescription = "";
                var channel = new NotificationChannel(CommonConstant.DIR_PATH, channelName, NotificationImportance.Default)
                {
                    Description = channelDescription
                };

                var notificationManager = (NotificationManager) GetSystemService();
                notificationManager.CreateNotificationChannel(channel);

                notificationManager.CreateNotificationChannel(channel);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CommonConstant.DIR_PATH)
                                            .SetContentTitle($"SS")
                                            .SetContentText("Hello World! This is my first notification!");
                                            
                Notification notification = builder.Build();

                // Publish the notification:
                const int notificationId = 0;
                notificationManager.Notify(notificationId, notification);
                */
            }
        }

        
    }
}
