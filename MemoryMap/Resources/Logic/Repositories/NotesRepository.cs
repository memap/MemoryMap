﻿# region References
using System;
using System.Collections.Generic;
using Android.Gms.Location;
using Android.Gms.Maps.Model;
using MemoryMap.Resources.Logic.Interfaces;

#endregion


namespace MemoryMap.Resources.Logic.Repositories
{
    public class NotesRepository : Repository<Note>, IRepository<Note>

    {
        private MainActivity mainActivity;
        public event Action OnItemsChanged;

        public NotesRepository(string fileName):base(fileName) {
            RestoreData();
            OnItemsUpdated += SaveData;
            OnItemsUpdated += RestoreData; 
            
        }

       

        public override void Add(Note item)
        {
            base.Add(item);
            OnItemsChanged?.Invoke();

        }

        public override void Remove(Note item)
        {
            base.Remove(item);
            OnItemsChanged?.Invoke();
        }
    }

}
