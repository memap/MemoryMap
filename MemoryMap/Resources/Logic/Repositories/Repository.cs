﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MemoryMap.Resources.Logic.Interfaces;
using MemoryMap.Resources.Logic.Converters;
using System.Threading.Tasks;
using MemoryMap.Resources.Logic.Models;
using Xamarin.Essentials;
using Newtonsoft.Json;
namespace MemoryMap.Resources.Logic.Repositories
{
    public class Repository<T> : IRepository<T>

        
    {
        public List<T> Items  { get => _items; }
        protected LocalJsonConverter<T> _JSONconverter;
        protected List<T> _items;
        protected readonly string _fileName;

        public event Action OnItemsUpdated;
        
        

        public Repository(string file_name)
        {
            _fileName = file_name;
            _items = new List<T>();
            _JSONconverter = new LocalJsonConverter<T>();
        }
            

        public virtual void Add(T item)
        {
            _items.Add(item);
            OnItemsUpdated?.Invoke();
        }

        public virtual void Remove(T item)
        {
            _items.Remove(item);
            OnItemsUpdated?.Invoke();
        }

        #region Helper methods



        public async void SaveData()
        {
            using (var stream = await FileSystem.OpenAppPackageFileAsync(_fileName))
            {
                using (var reader = new StreamReader(stream))
                {
                    var data = _JSONconverter.FromSource(_items);
                    await File.WriteAllTextAsync(Path.Combine(CommonConstant.DIR_PATH, _fileName), data);
                }
            }
        }


        public async void RestoreData()
        {
            
                using (var stream = await FileSystem.OpenAppPackageFileAsync(_fileName))
                {
                    using (var reader = new StreamReader(stream))
                    {
                        using (var jr = new JsonTextReader(reader))
                        {
                            var serializer = new JsonSerializer();
                            _items = serializer.Deserialize<List<T>>(jr);
                        }
                    }
                }
            

       }



        

        #endregion
    }
}
