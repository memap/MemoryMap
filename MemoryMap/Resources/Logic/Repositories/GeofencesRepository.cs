﻿#region References
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Android.Gms.Location;
using MemoryMap.Resources.Logic.Interfaces;
using MemoryMap.Resources.Logic.Converters;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
#endregion


namespace MemoryMap.Resources.Logic.Repositories
{
    public class GeofencesRepository : Repository<IGeofence>, IRepository<IGeofence>

    {
        private readonly string _filePath;
        public event Action<string> OnItemsChanged;
        private GeofenceBuilder geofenceBuilder;

        public GeofencesRepository(string filePath)
        {
            _filePath = filePath;
            OnItemsChanged += SaveData;
            geofenceBuilder = new GeofenceBuilder();
            geofenceBuilder  
                        .SetExpirationDuration(Geofence.NeverExpire)
                        .SetTransitionTypes(Geofence.GeofenceTransitionEnter | Geofence.GeofenceTransitionExit);
        }

        public override void Add(IGeofence item)
        {
           

            base.Add(item);
            OnItemsChanged?.Invoke(_filePath);
            
        }

        public override void Remove(IGeofence item)
        {
            base.Remove(item);
            OnItemsChanged?.Invoke(_filePath);
        }

        const float RADIUS = 350;

        /*public IGeofence CreateGeofence(LatLng latLng)
        {
            IGeofence geofence = geofenceBuilder
                        .SetCircularRegion(latLng.Latitude, latLng.Longitude, RADIUS)
                        .Build();
            return geofence;

        }
        */
    }   
        
}
