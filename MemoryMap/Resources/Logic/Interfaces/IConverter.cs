﻿using System;
using System.Collections.Generic;

namespace MemoryMap.Resources.Logic.Interfaces
{
    public interface IConverter<T>

    {
            string FromSource(List<T> sourve);
            List<T> ToSource(string data);

    }
    
}
