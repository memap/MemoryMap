﻿using System;
using System.Collections.Generic;



namespace MemoryMap.Resources.Logic.Interfaces
{
    public interface IRepository<T>
    {
        List<T> Items { get; }
        
        #region Events
        event Action OnItemsUpdated;

        #endregion

        void Add(T item);
        void Remove(T item);
        void SaveData();
        void RestoreData();



    }
}
