﻿using System;
using System.IO;
using Xamarin.Essentials;

namespace MemoryMap.Resources.Logic.Models
{
    public class CommonConstant
    {

        public static long TWO_MINUTES_INTERVAL = 200;
        public static long FIVE_MINUTES_INTERVAL = 500;
        public static string DIR_PATH = FileSystem.AppDataDirectory;
        public static string FILE_NAME_NOTES = "notes.json";
    }
}
