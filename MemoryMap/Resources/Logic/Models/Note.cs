﻿using System;
using System.Collections.Generic;
using Android.Gms.Location;
using Android.Gms.Location.Places;
using Android.Gms.Maps.Model;
using GoogleApi.Entities.Common;
using Xamarin.Essentials;
using Location = Xamarin.Essentials.Location;
using Newtonsoft.Json;

namespace MemoryMap.Resources.Logic
{
    public enum Topic
        {
            Family,
            Shopping,
            Job,
            HealthCare,
            
        }
    [JsonObjectAttribute]
    public class Note
    {
        #region Properties
        
        public DateTime CreatedDt { get; private set; }
        public DateTime LastChangedDt { get; set; }
        public Topic Topic { get; set; }
        public string Title { get; set; }
        public string NoteContent { get; set; }
        public Location Location { get; set; }

        #endregion

        public Note(DateTime createdDt, Topic topic,string title, string content, Location latLng)
            => (CreatedDt, LastChangedDt, Topic, Title, NoteContent, Location) = (createdDt, createdDt, topic, title, content, latLng);
        


    }
}
