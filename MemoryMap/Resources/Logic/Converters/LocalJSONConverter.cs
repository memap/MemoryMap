﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using MemoryMap.Resources.Logic.Interfaces;

namespace MemoryMap.Resources.Logic.Converters

{
    public class LocalJsonConverter<T> : IConverter<T>
    {
       
        public List<T> ToSource(string data)
        {
            JsonConvert.SerializeObject(data);
            return JsonConvert.DeserializeObject<List<T>>(data);
        }

        public string FromSource(List<T> source)
            => JsonConvert.SerializeObject(source);

    }
}