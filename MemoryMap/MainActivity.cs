﻿using System;
using Android.App;
using Android.Gms.Maps;
using Android.Gms.Common;
using Android.Gms.Location;
using Android.Locations;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using GoogleApi.Entities.Maps;
using Android.Appwidget;
using Android.Util;
using MemoryMap.Resources.Logic.Repositories;
using Android.Content;
using MemoryMap.Resources;
using Android.Gms.Tasks;
using MemoryMap.Resources.Logic.Models;
using MemoryMap.Resources.Logic;

namespace MemoryMap
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        #region Const
        long TWO_MINUTES_INTERVAL = 200;
        long FIVE_MINUTES_INTERVAL = 500;
        #endregion

        static MainActivity Instance;
        public static MainActivity GetInstance()
            => Instance;


        FusedLocationProviderClient locationProviderClient;
        GeofencingClient geofencingClient;
        LocationRequest locacationRequest;
        public NotesRepository notes;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Instance = this;
            notes = new NotesRepository(CommonConstant.FILE_NAME_NOTES);
            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            notes.Add(new Note(DateTime.Now, Topic.Job, "Working", "Youp", new Xamarin.Essentials.Location(35.938325, -120.233578)));
            UpdateLocation();
            
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
     

        #region Location Requesting
        private void UpdateLocation()
        {

            BuildLocationRequest();
            
            locationProviderClient = LocationServices.GetFusedLocationProviderClient(this);
            locationProviderClient.RequestLocationUpdates(locacationRequest, GetPendingIntent());
            
       }
     

        private PendingIntent GetPendingIntent()
        {
            Intent intent = new Intent(this, typeof(MMLocationBroadcastReceiver));
            intent.SetAction(MMLocationBroadcastReceiver.ACTION_PROCESS_LOCATION);
            return PendingIntent.GetBroadcast(this, 0, intent, PendingIntentFlags.UpdateCurrent);
        }

        private void BuildLocationRequest()
        {
            locacationRequest = new LocationRequest();
            locacationRequest.SetPriority(LocationRequest.PriorityHighAccuracy)
                .SetInterval(FIVE_MINUTES_INTERVAL)
                .SetFastestInterval(TWO_MINUTES_INTERVAL)
                .SetSmallestDisplacement(10f);
        }

        #endregion
       

       

        #region Source code


        bool IsGooglePlayServicesInstalled()
        {
            var queryResult = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (queryResult == ConnectionResult.Success)
            {
                Log.Info("MainActivity", "Google Play Services is installed on this device.");
                return true;
            }

            if (GoogleApiAvailability.Instance.IsUserResolvableError(queryResult))
            {
                // Check if there is a way the user can resolve the issue
                var errorString = GoogleApiAvailability.Instance.GetErrorString(queryResult);
                Log.Error("MainActivity", "There is a problem with Google Play Services on this device: {0} - {1}",
                          queryResult, errorString);

                // Alternately, display the error to the user.
            }

            return false;


        }

        #endregion



    }
}

